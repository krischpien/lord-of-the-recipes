package krist.cz.lordoftherecipes.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.Bind;
import butterknife.ButterKnife;
import javax.inject.Inject;
import krist.cz.lordoftherecipes.R;
import krist.cz.lordoftherecipes.application.LOTRApplication;
import krist.cz.lordoftherecipes.domain.entity.RecipeTopic;
import krist.cz.lordoftherecipes.presenter.RecipeTopicDetailPresenter;
import krist.cz.lordoftherecipes.view.RecipeTopicDetailView;
import krist.cz.lordoftherecipes.view.activity.RecipeTopicDetailActivity;
import krist.cz.lordoftherecipes.view.activity.RecipeTopicListActivity;

/**
 * A fragment representing a single RecipeTopic detail screen.
 * This fragment is either contained in a {@link RecipeTopicListActivity}
 * in two-pane mode (on tablets) or a {@link RecipeTopicDetailActivity}
 * on handsets.
 */
public class RecipeTopicDetailFragment extends Fragment implements RecipeTopicDetailView {

  public static final String ARG_ITEM_ID = "item_id";
  public static final String PARCEL_KEY_TOPIC_DETAIL = "parcel_topic_detail";

  @Inject RecipeTopicDetailPresenter topicDetailPresenter;
  @Bind(R.id.tv_topic_title) TextView tvTopicTitle;
  @Bind(R.id.tv_topic_link) TextView tvTopicLink;
  private CollapsingToolbarLayout appBarLayout;

  private RecipeTopic recipeTopicHolder = null;

  public RecipeTopicDetailFragment() {
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setupInjections();
  }

  @Override public void onActivityCreated(@Nullable Bundle savedInstanceState) {
    super.onActivityCreated(savedInstanceState);
    if(savedInstanceState != null){
      recipeTopicHolder = savedInstanceState.getParcelable(PARCEL_KEY_TOPIC_DETAIL);
      renderTopicDetail(recipeTopicHolder);
    }
    else if (getArguments().containsKey(ARG_ITEM_ID)) {
      long topicId = getArguments().getLong(ARG_ITEM_ID);
      topicDetailPresenter.loadTopicDetail(topicId);
    }
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putParcelable(PARCEL_KEY_TOPIC_DETAIL, recipeTopicHolder);
  }

  private void setupInjections() {
    ((LOTRApplication) getActivity().getApplication()).getPresenterComponent().inject(this);
    topicDetailPresenter.setDetailView(this);


  }

  private void setToolBarTitle(String toolBarTitle){
    if (appBarLayout != null) {
      appBarLayout.setTitle(toolBarTitle);
    }
  }

  @Override public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View rootView = inflater.inflate(R.layout.fragment_recipetopic_detail, container, false);
    ButterKnife.bind(this, rootView);
    appBarLayout = (CollapsingToolbarLayout) getActivity().findViewById(R.id.toolbar_layout);
    return rootView;
  }

  @Override public void renderTopicDetail(RecipeTopic recipeTopic) {
    setToolBarTitle(recipeTopic.getTitle());
    recipeTopicHolder = recipeTopic;
    tvTopicTitle.setText(Html.fromHtml(recipeTopic.getTitle()));
    tvTopicLink.setText(getResources().getString(R.string.topic_link_text, recipeTopic.getLink()));
  }

  @Override public void showProgress() {

  }

  @Override public void hideProgress() {

  }

  @Override public void showMessage(String message) {

  }

  @Override public void onError(String message) {

  }

  @Override public void onDestroyView() {
    super.onDestroyView();
    ButterKnife.unbind(this);
  }
}
