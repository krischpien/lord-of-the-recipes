package krist.cz.lordoftherecipes.view;

import krist.cz.lordoftherecipes.domain.entity.RecipeTopic;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 23.05.2016.
 */

public interface RecipeTopicDetailView extends LoadingView {

  void renderTopicDetail(RecipeTopic recipeTopic);
}
