package krist.cz.lordoftherecipes.view;

import java.util.List;
import krist.cz.lordoftherecipes.domain.entity.RecipeTopic;
import krist.cz.lordoftherecipes.view.fragment.RecipeTopicDetailFragment;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
public interface RecipeTopicsView extends LoadingView {
  void renderTopics(List<RecipeTopic> recipeTopics);
  void showDetailFragment(RecipeTopicDetailFragment recipeTopicDetailFragment);
  boolean isInTwoPaneMode();

}
