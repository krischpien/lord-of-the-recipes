package krist.cz.lordoftherecipes.view.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import butterknife.Bind;
import butterknife.ButterKnife;
import krist.cz.lordoftherecipes.R;
import krist.cz.lordoftherecipes.view.fragment.RecipeTopicDetailFragment;

public class RecipeTopicDetailActivity extends AppCompatActivity {

  @Bind(R.id.detail_toolbar) Toolbar toolbar;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_recipetopic_detail);
    ButterKnife.bind(this);
    setupToolbar();


    if (savedInstanceState == null) {
      // Create the detail fragment and add it to the activity
      // using a fragment transaction.
      Bundle arguments = new Bundle();
      arguments.putLong(RecipeTopicDetailFragment.ARG_ITEM_ID,
          getIntent().getLongExtra(RecipeTopicDetailFragment.ARG_ITEM_ID, 0L));
      RecipeTopicDetailFragment fragment = new RecipeTopicDetailFragment();
      fragment.setArguments(arguments);
      getSupportFragmentManager().beginTransaction()
          .add(R.id.nsv_recipetopic_detail_container, fragment)
          .commit();
    }
  }

  private void setupToolbar() {
    setSupportActionBar(toolbar);
    ActionBar actionBar = getSupportActionBar();
    if (actionBar != null) {
      actionBar.setDisplayHomeAsUpEnabled(true);
    }
  }
}
