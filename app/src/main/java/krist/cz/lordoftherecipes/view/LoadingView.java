package krist.cz.lordoftherecipes.view;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
public interface LoadingView extends BaseView {

  void showProgress();

  void hideProgress();
}
