package krist.cz.lordoftherecipes.view;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
public interface BaseView {
  void showMessage(String message);

  void onError(String message);
}
