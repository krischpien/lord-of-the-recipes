package krist.cz.lordoftherecipes.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import krist.cz.lordoftherecipes.application.LOTRApplication;
import krist.cz.lordoftherecipes.R;
import krist.cz.lordoftherecipes.domain.entity.RecipeTopic;
import krist.cz.lordoftherecipes.presenter.RecipeTopicsPresenter;
import krist.cz.lordoftherecipes.view.RecipeTopicsView;
import krist.cz.lordoftherecipes.view.adapter.RecipeTopicAdapter;
import krist.cz.lordoftherecipes.view.fragment.RecipeTopicDetailFragment;

public class RecipeTopicListActivity extends AppCompatActivity implements RecipeTopicsView {

  private static final String TAG = "TopicListActivity";
  public static final String PARCEL_KEY_TOPIC_LIST = "key";

  @Bind(R.id.rv_topic_list) RecyclerView rvRecipetopicList;
  @Bind(R.id.toolbar) Toolbar toolbar;
  @Bind(R.id.fab) FloatingActionButton fab;
  @Bind(R.id.nsv_recipetopic_detail_container) @Nullable NestedScrollView nsvTopicDetail;
  @Inject RecipeTopicsPresenter recipeTopicsPresenter;
    @Bind(R.id.clRootView) CoordinatorLayout clRootView;

  private int totalItemCount;
  private int visibleItemCount;
  private int firstVisibleItem;
  private int previousTotal;
  private boolean loadNext = true;

  private RecipeTopicAdapter recipeTopicAdapter = null;
  private ArrayList<RecipeTopic> topicList = new ArrayList<>();
  private boolean twoPaneMode = false;

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_recipetopic_list);
    ButterKnife.bind(this);

    setupInjections();
    setupToolbar();
    detectTwoPane();
    setupTopicRecycler(topicList);

    if (savedInstanceState == null) {
      recipeTopicsPresenter.loadNextTopics();
    } else {
      topicList = savedInstanceState.getParcelableArrayList(PARCEL_KEY_TOPIC_LIST);
      renderTopics(topicList);
    }
  }

  @Override protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    ArrayList<RecipeTopic> recipeTopics =
        (ArrayList<RecipeTopic>) recipeTopicAdapter.getRecipeTopics();
    outState.putParcelableArrayList(PARCEL_KEY_TOPIC_LIST, recipeTopics);
  }

  private void setupTopicRecycler(List<RecipeTopic> topicList) {
    recipeTopicAdapter = new RecipeTopicAdapter(topicList);
    recipeTopicAdapter.setOnRecipeTopicClickListener(
        new RecipeTopicAdapter.OnRecipeTopicClickListener() {
          @Override public void onRecipeTopicClick(RecipeTopic recipeTopic, View view) {
            long topicId = recipeTopic.getId();
            recipeTopicsPresenter.showTopicDetail(topicId,
                RecipeTopicListActivity.this);
          }
        });
    rvRecipetopicList.setAdapter(recipeTopicAdapter);
    rvRecipetopicList.setLayoutManager(new LinearLayoutManager(this));
    rvRecipetopicList.addOnScrollListener(new RecyclerView.OnScrollListener() {
      @Override public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
      }

      @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager linearLayoutManager =
            (LinearLayoutManager) recyclerView.getLayoutManager();

        if (recipeTopicsPresenter.isLoading()) {
          return;
        }

        totalItemCount = linearLayoutManager.getItemCount();
        visibleItemCount = linearLayoutManager.getChildCount();
        firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

        if (loadNext) {
          if (totalItemCount > previousTotal) {
            loadNext = false;
            previousTotal = totalItemCount;
          }
        }
        if (!loadNext && (totalItemCount - visibleItemCount) <= (firstVisibleItem
            + RecipeTopicsPresenter.DEFAULT_PAGE_SIZE)) {
          // End has been reached
          recipeTopicsPresenter.loadNextTopics();
          loadNext = true;
        }

        if (recipeTopicsPresenter.canLoadNext()
            && linearLayoutManager.findLastCompletelyVisibleItemPosition() == totalItemCount - 1) {
          showProgress();
        }
      }
    });
  }

  private void detectTwoPane() {
    if (nsvTopicDetail != null) {
      twoPaneMode = true;
    }
  }

  private void setupInjections() {
    ButterKnife.bind(this); //view injections
    ((LOTRApplication) getApplication()).getPresenterComponent().inject(this); //presenter injection
    recipeTopicsPresenter.setRecipeTopicsView(this);
  }

  private void setupToolbar() {
    setSupportActionBar(toolbar);
    toolbar.setTitle(getTitle());
  }

  @Override public void renderTopics(List<RecipeTopic> recipeTopics) {
    recipeTopicAdapter.getRecipeTopics().addAll(recipeTopics);
    recipeTopicAdapter.notifyDataSetChanged();
  }

  @Override public void showProgress() {
    //pbLoadingProgress.setVisibility(View.VISIBLE);
    //TODO: show progress as another viewType in recycler
  }

  @Override public void hideProgress() {

  }

  @Override public void showMessage(String message) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
  }

  @Override public void onError(String message) {
    Snackbar.make(clRootView, message, Snackbar.LENGTH_INDEFINITE)
        .setAction("Retry", new View.OnClickListener() {
          @Override public void onClick(View v) {
            recipeTopicsPresenter.loadNextTopics();
          }
        })
        .show();
  }

  @Override public boolean isInTwoPaneMode() {
    return twoPaneMode;
  }

  @Override public void showDetailFragment(RecipeTopicDetailFragment recipeTopicDetailFragment) {
    if (twoPaneMode) {
      replaceRightPaneFragment(recipeTopicDetailFragment);
    } else {
      Log.d(TAG, "Cannot replace fragment, because activity is in single pane mode!");
    }
  }

  public void replaceRightPaneFragment(Fragment fragment) {
    getSupportFragmentManager().beginTransaction()
        .replace(R.id.nsv_recipetopic_detail_container, fragment)
        .commit();
  }
}
