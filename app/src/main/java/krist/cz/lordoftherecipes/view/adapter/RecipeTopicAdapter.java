package krist.cz.lordoftherecipes.view.adapter;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.Bind;
import butterknife.ButterKnife;
import java.util.ArrayList;
import java.util.List;
import krist.cz.lordoftherecipes.R;
import krist.cz.lordoftherecipes.domain.entity.RecipeTopic;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
public class RecipeTopicAdapter
    extends RecyclerView.Adapter<RecipeTopicAdapter.RecipeTopicViewHolder> {

  private List<RecipeTopic> recipeTopics = new ArrayList<>();
  private OnRecipeTopicClickListener onRecipeTopicClickListener = null;

  public RecipeTopicAdapter(List<RecipeTopic> recipeTopics) {
    this.recipeTopics = recipeTopics;
  }

  public void setOnRecipeTopicClickListener(OnRecipeTopicClickListener onRecipeTopicClickListener) {
    this.onRecipeTopicClickListener = onRecipeTopicClickListener;
  }

  public List<RecipeTopic> getRecipeTopics() {
    return recipeTopics;
  }

  public void setRecipeTopics(List<RecipeTopic> recipeTopics) {
    this.recipeTopics = recipeTopics;
  }

  @Override public RecipeTopicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    View itemView =
        LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recipe_topic, parent, false);
    return new RecipeTopicViewHolder(itemView);
  }

  @Override public void onBindViewHolder(final RecipeTopicViewHolder holder, int position) {
    final RecipeTopic recipeTopic = recipeTopics.get(position);
    holder.tvTopicTitle.setText(Html.fromHtml(recipeTopic.getTitle()));
    if(onRecipeTopicClickListener != null){
      holder.cvItemContainer.setOnClickListener(new View.OnClickListener() {
        @Override public void onClick(View v) {
          onRecipeTopicClickListener.onRecipeTopicClick(recipeTopic, v);
        }
      });
    }
  }

  @Override public int getItemCount() {
    return recipeTopics.size();
  }

  static class RecipeTopicViewHolder extends RecyclerView.ViewHolder {
    @Bind(R.id.tv_topic_title) TextView tvTopicTitle;
    @Bind(R.id.cv_item_container) CardView cvItemContainer;

    public RecipeTopicViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }

  public interface OnRecipeTopicClickListener{
    void onRecipeTopicClick(RecipeTopic recipeTopic, View view);
  }
}
