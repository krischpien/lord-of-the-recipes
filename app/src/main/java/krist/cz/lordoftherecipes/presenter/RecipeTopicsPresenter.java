package krist.cz.lordoftherecipes.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import java.util.List;
import javax.inject.Inject;
import krist.cz.lordoftherecipes.application.LOTRApplication;
import krist.cz.lordoftherecipes.domain.dto.ResponseWrapper;
import krist.cz.lordoftherecipes.domain.entity.RecipeTopic;
import krist.cz.lordoftherecipes.domain.repository.RecipeServiceRetrofit;
import krist.cz.lordoftherecipes.view.RecipeTopicsView;
import krist.cz.lordoftherecipes.view.activity.RecipeTopicDetailActivity;
import krist.cz.lordoftherecipes.view.fragment.RecipeTopicDetailFragment;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
public class RecipeTopicsPresenter {

  public static final String TAG = "RecipeTopicsPresenter";

  public static final int DEFAULT_PAGE_SIZE = 5;
  public static final String DEFAULT_TOPIC = "cooking";

  public int nextPage = 1;
  private boolean canLoadNext = true;
  private boolean loading = false;

  @Inject RecipeServiceRetrofit recipeService;
  private RecipeTopicsView recipeTopicsView;

  public RecipeTopicsPresenter(LOTRApplication application) {
    application.getApiServiceComponent().inject(this);
  }

  public RecipeTopicsView getRecipeTopicsView() {
    return recipeTopicsView;
  }

  public void setRecipeTopicsView(RecipeTopicsView recipeTopicsView) {
    this.recipeTopicsView = recipeTopicsView;
  }

  public boolean canLoadNext() {
    return canLoadNext;
  }

  public boolean isLoading() {
    return loading;
  }

  public void loadNextTopics() {
    if (!canLoadNext) return;
    if (loading) {
      recipeTopicsView.showMessage("loading...");
    }

    recipeService.getTopics(DEFAULT_TOPIC, DEFAULT_PAGE_SIZE, nextPage)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(new Subscriber<ResponseWrapper<List<RecipeTopic>>>() {
          @Override public void onCompleted() {
            loading = false;
            nextPage++;
            recipeTopicsView.hideProgress();
          }

          @Override public void onError(Throwable e) {
            recipeTopicsView.onError(e.getLocalizedMessage());
            Log.e(TAG, e.getMessage());
          }

          @Override public void onNext(ResponseWrapper<List<RecipeTopic>> listResponseWrapper) {
            recipeTopicsView.renderTopics(listResponseWrapper.getData());

            if (!listResponseWrapper.hasMore()) {
              canLoadNext = false;
            }
          }
        });
  }

  public void showTopicDetail(long id, Context context){
    if(recipeTopicsView.isInTwoPaneMode()){
      RecipeTopicDetailFragment recipeTopicDetailFragment = new RecipeTopicDetailFragment();
      Bundle bundle = new Bundle();
      bundle.putLong(RecipeTopicDetailFragment.ARG_ITEM_ID, id);
      recipeTopicDetailFragment.setArguments(bundle);
      recipeTopicsView.showDetailFragment(recipeTopicDetailFragment);
    } else {
      Intent topicDetailIntent = new Intent(context, RecipeTopicDetailActivity.class);
      topicDetailIntent.putExtra(RecipeTopicDetailFragment.ARG_ITEM_ID, id);
      context.startActivity(topicDetailIntent);
    }
  }
}
