package krist.cz.lordoftherecipes.presenter;

import android.util.Log;
import java.util.List;
import javax.inject.Inject;
import krist.cz.lordoftherecipes.application.LOTRApplication;
import krist.cz.lordoftherecipes.domain.dto.ResponseWrapper;
import krist.cz.lordoftherecipes.domain.entity.RecipeTopic;
import krist.cz.lordoftherecipes.domain.repository.RecipeServiceRetrofit;
import krist.cz.lordoftherecipes.view.RecipeTopicDetailView;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 23.05.2016.
 */

public class RecipeTopicDetailPresenter {

  public static final String TAG = "RecipeTopicsPresenter";

  @Inject RecipeServiceRetrofit recipeService;
  private RecipeTopicDetailView detailView;

  public RecipeTopicDetailPresenter(LOTRApplication application) {
    application.getApiServiceComponent().inject(this);
  }

  public void setDetailView(RecipeTopicDetailView detailView) {
    this.detailView = detailView;
  }

  public void loadTopicDetail(long id) {
    recipeService.getTopicDetail(id, "cooking")
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.newThread())
        .subscribe(new Subscriber<ResponseWrapper<List<RecipeTopic>>>() {
          @Override public void onCompleted() {
            Log.d(TAG, "Request completed!");
          }

          @Override public void onError(Throwable e) {
            detailView.onError(e.getMessage());
          }

          @Override public void onNext(ResponseWrapper<List<RecipeTopic>> listResponseWrapper) {
            RecipeTopic recipeTopic = listResponseWrapper.getData().get(0);// single item list
            detailView.renderTopicDetail(recipeTopic);
          }
        });


  }
}
