package krist.cz.lordoftherecipes.application.di.module;

import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import krist.cz.lordoftherecipes.application.LOTRApplication;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
@Module public class ApplicationModule {

  private LOTRApplication application;

  public ApplicationModule(LOTRApplication application) {
    this.application = application;
  }

  @Provides @Singleton LOTRApplication provideApplication() {
    return application;
  }
}
