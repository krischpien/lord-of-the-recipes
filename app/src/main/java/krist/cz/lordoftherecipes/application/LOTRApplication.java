package krist.cz.lordoftherecipes.application;

import android.app.Application;
import krist.cz.lordoftherecipes.application.di.component.ApiServiceComponent;
import krist.cz.lordoftherecipes.application.di.component.DaggerApiServiceComponent;
import krist.cz.lordoftherecipes.application.di.component.DaggerPresenterComponent;
import krist.cz.lordoftherecipes.application.di.component.PresenterComponent;
import krist.cz.lordoftherecipes.application.di.module.ApiServiceModule;
import krist.cz.lordoftherecipes.application.di.module.ApplicationModule;
import krist.cz.lordoftherecipes.application.di.module.NetworkModule;
import krist.cz.lordoftherecipes.application.di.module.PresenterModule;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
public class LOTRApplication extends Application {

  private ApiServiceComponent apiServiceComponent;
  private PresenterComponent presenterComponent;

  @Override public void onCreate() {
    super.onCreate();
    initDependencyInjection();
  }

  private void initDependencyInjection() {
    apiServiceComponent = DaggerApiServiceComponent.builder()
        .applicationModule(new ApplicationModule(this))
        .networkModule(new NetworkModule())
        .apiServiceModule(new ApiServiceModule())
        .build();

    presenterComponent = DaggerPresenterComponent.builder()
        .presenterModule(new PresenterModule())
        .apiServiceComponent(apiServiceComponent)
        .build();
  }

  public ApiServiceComponent getApiServiceComponent() {
    return apiServiceComponent;
  }

  public PresenterComponent getPresenterComponent() {
    return presenterComponent;
  }
}
