package krist.cz.lordoftherecipes.application.di.scope;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.inject.Scope;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
@Scope @Documented @Retention(RetentionPolicy.RUNTIME) public @interface UserScope {
}
