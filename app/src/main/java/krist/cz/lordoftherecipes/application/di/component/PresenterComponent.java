package krist.cz.lordoftherecipes.application.di.component;

import dagger.Component;
import krist.cz.lordoftherecipes.application.di.module.PresenterModule;
import krist.cz.lordoftherecipes.application.di.scope.UserScope;
import krist.cz.lordoftherecipes.view.activity.RecipeTopicListActivity;
import krist.cz.lordoftherecipes.view.fragment.RecipeTopicDetailFragment;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
@Component(modules = PresenterModule.class, dependencies = ApiServiceComponent.class) @UserScope
public interface PresenterComponent {

  void inject(RecipeTopicListActivity recipeTopicListActivity);
  void inject(RecipeTopicDetailFragment recipeTopicDetailFragment);
}
