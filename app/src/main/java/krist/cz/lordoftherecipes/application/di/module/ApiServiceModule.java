package krist.cz.lordoftherecipes.application.di.module;

import dagger.Module;
import dagger.Provides;
import krist.cz.lordoftherecipes.domain.repository.RecipeServiceRetrofit;
import retrofit.Retrofit;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
@Module public class ApiServiceModule {

  @Provides RecipeServiceRetrofit provideRecipeService(Retrofit retrofit) {
    return retrofit.create(RecipeServiceRetrofit.class);
  }
}
