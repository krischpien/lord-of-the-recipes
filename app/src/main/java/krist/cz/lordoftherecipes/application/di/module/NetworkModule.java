package krist.cz.lordoftherecipes.application.di.module;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.Cache;
import com.squareup.okhttp.OkHttpClient;
import dagger.Module;
import dagger.Provides;
import krist.cz.lordoftherecipes.application.LOTRApplication;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
@Module public class NetworkModule {
  private static final String TAG = "NetworkModule";

  @Provides Cache provideOkHttpCache(LOTRApplication application) {
    int cacheSize = 10 * 1024 * 1024; // 10 MiB
    Cache cache = new Cache(application.getCacheDir(), cacheSize);
    return cache;
  }

  @Provides Gson provideGson() {
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
    return gsonBuilder.create();
  }

  @Provides OkHttpClient provideOkHttpClient(Cache cache) {
    OkHttpClient client = new OkHttpClient();
    client.setCache(cache);
    //client.interceptors().add(new Interceptor() {
    //  @Override
    //  public Response intercept(Interceptor.Chain chain) throws IOException {
    //    Request request = chain.request();
    //    HttpUrl url = request.httpUrl().newBuilder()
    //
    //        .build();
    //    request = request.newBuilder().url(url).build();
    //    return chain.proceed(request);
    //  }
    //});
    return client;
  }

  @Provides Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
    Retrofit retrofit = new Retrofit.Builder().baseUrl("https://api.stackexchange.com/2.2/")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .client(okHttpClient)
        .build();
    return retrofit;
  }
}
