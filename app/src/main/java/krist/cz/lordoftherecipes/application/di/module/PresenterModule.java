package krist.cz.lordoftherecipes.application.di.module;

import dagger.Module;
import dagger.Provides;
import krist.cz.lordoftherecipes.application.LOTRApplication;
import krist.cz.lordoftherecipes.application.di.scope.UserScope;
import krist.cz.lordoftherecipes.presenter.RecipeTopicDetailPresenter;
import krist.cz.lordoftherecipes.presenter.RecipeTopicsPresenter;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
@Module public class PresenterModule {

  @Provides @UserScope RecipeTopicsPresenter provideRecipeTopicsPresenter(
      LOTRApplication application) {
    return new RecipeTopicsPresenter(application);
  }

  @Provides @UserScope RecipeTopicDetailPresenter provideRecipeTopicsDetailPresenter(
      LOTRApplication application) {
    return new RecipeTopicDetailPresenter(application);
  }
}
