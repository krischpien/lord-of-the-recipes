package krist.cz.lordoftherecipes.application.di.component;

import dagger.Component;
import javax.inject.Singleton;
import krist.cz.lordoftherecipes.application.LOTRApplication;
import krist.cz.lordoftherecipes.application.di.module.ApiServiceModule;
import krist.cz.lordoftherecipes.application.di.module.ApplicationModule;
import krist.cz.lordoftherecipes.application.di.module.NetworkModule;
import krist.cz.lordoftherecipes.presenter.RecipeTopicDetailPresenter;
import krist.cz.lordoftherecipes.presenter.RecipeTopicsPresenter;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
@Component(modules = {
    ApplicationModule.class, NetworkModule.class, ApiServiceModule.class
}) @Singleton public interface ApiServiceComponent {

  LOTRApplication application();

  void inject(RecipeTopicsPresenter recipeTopicsPresenter);
  void inject(RecipeTopicDetailPresenter recipeTopicDetailPresenter);
}
