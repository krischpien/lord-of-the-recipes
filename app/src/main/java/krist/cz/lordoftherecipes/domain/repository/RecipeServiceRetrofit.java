package krist.cz.lordoftherecipes.domain.repository;

import java.util.List;
import krist.cz.lordoftherecipes.domain.dto.ResponseWrapper;
import krist.cz.lordoftherecipes.domain.entity.RecipeTopic;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
public interface RecipeServiceRetrofit {
  /**
   *
   * @param site
   * @param pageSize
   * @param page
   * @return
   */
  @GET("questions") Observable<ResponseWrapper<List<RecipeTopic>>> getTopics(
      @Query("site") String site, @Query("pagesize") int pageSize, @Query("page") int page);

  @GET("questions/{topicId}") Observable<ResponseWrapper<List<RecipeTopic>>> getTopicDetail(
      @Path("topicId") long topicId, @Query("site") String site);
}
