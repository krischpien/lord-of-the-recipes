package krist.cz.lordoftherecipes.domain.entity;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
public class RecipeTopic implements Parcelable, Serializable {

  @SerializedName("question_id")
  private long id;
  private String title;
  private String link;
  private String body;

  public RecipeTopic() {

  }

  public RecipeTopic(Parcel in) {
    this.title = in.readString();
    this.link = in.readString();
    this.body = in.readString();
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getLink() {
    return link;
  }

  public void setLink(String link) {
    this.link = link;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }

  public static final Parcelable.Creator<RecipeTopic> CREATOR =
      new Parcelable.Creator<RecipeTopic>() {
        public RecipeTopic createFromParcel(Parcel in) {
          return new RecipeTopic(in);
        }

        @Override public RecipeTopic[] newArray(int size) {
          return new RecipeTopic[size];
        }
      };

  @Override public int describeContents() {
    return 0;
  }

  @Override public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(title);
    dest.writeString(link);
    dest.writeString(body);
  }
}
