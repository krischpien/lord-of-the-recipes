package krist.cz.lordoftherecipes.domain.dto;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 * Created by Jan Krist (jankristdev@gmail.com) on 22.05.2016.
 */
public class ResponseWrapper<T> implements Serializable {

  @SerializedName("items") private T data;

  /**
   * has_more : true
   * quota_max : 300
   * quota_remaining : 294
   */

  @SerializedName("has_more") private boolean more;
  @SerializedName("quota_max") private int quotaMax;
  @SerializedName("quota_remaining") private int quotaRemaining;

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }

  public boolean hasMore() {
    return more;
  }

  public void setMore(boolean more) {
    this.more = more;
  }

  public int getQuotaMax() {
    return quotaMax;
  }

  public void setQuotaMax(int quotaMax) {
    this.quotaMax = quotaMax;
  }

  public int getQuotaRemaining() {
    return quotaRemaining;
  }

  public void setQuotaRemaining(int quotaRemaining) {
    this.quotaRemaining = quotaRemaining;
  }
}
